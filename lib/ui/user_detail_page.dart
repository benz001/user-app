import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:user_app/controller/user_detail_controller.dart';
import 'package:user_app/helper/primary_button.dart';
import 'package:user_app/model/user_detail_model.dart';

class UserDetailPage extends StatelessWidget {
  static const routeName = '/user-detail-page';
  const UserDetailPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<UserDetailController>(builder: (controller) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Single User'),
          centerTitle: true,
        ),
        body: Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.grey[300],
          alignment: Alignment.center,
          child: Builder(builder: (context) {
            if (controller.isLoadingUserDetail) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              if (controller.isSuccessUserDetail) {
                return Column(
                  children: [_buildUserInfo(controller.userDetailModel.data),_buildSupportInfo(controller.userDetailModel.support)],
                );
              } else {
                return Center(
                  child: PrimaryButton(
                    text: 'Reload',
                    onPressed: () {
                      controller.getUserDetailProcess();
                    },
                  ),
                );
              }
            }
          }),
        ),
      );
    });
  }

  Widget _buildUserInfo(DataUserDetail dataUserDetail) {
    return Container(
      width: double.infinity,
      color: Colors.white,
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.only(top: 10),
      child: Row(
        children: [
          Container(
            width: 50,
            height: 50,
            color: Colors.grey,
            child: Image.network(dataUserDetail.avatar),
          ),
          const SizedBox(
            width: 5,
          ),
          Expanded(
            child: Container(
              color: Colors.transparent,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                      '${dataUserDetail.firstName} ${dataUserDetail.lastName}', style: const TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(dataUserDetail.email, style: const TextStyle(fontSize: 14, fontWeight: FontWeight.bold))
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildSupportInfo(SupportDetail support) {
    return Expanded(
      child: Container(
        width: double.infinity,
        color: Colors.white,
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.only(top: 10),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: double.infinity,
                color: Colors.transparent,
                child: const Text('Support', style:  TextStyle(fontSize: 14, fontWeight: FontWeight.bold),)),
              Container(
                width: double.infinity,
                color: Colors.transparent,
                child: Text(support.text)),
            ],
          ),
        )
      ),
    );
  }
}
