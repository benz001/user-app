import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:user_app/controller/list_user_controller.dart';
import 'package:user_app/controller/main_controller.dart';
import 'package:user_app/helper/primary_button.dart';
import 'package:user_app/model/user_model.dart';
import 'package:user_app/ui/user_detail_page.dart';

class ListUserPage extends StatelessWidget {
  const ListUserPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ListUserController>(builder: (controller) {
      return Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.grey[300],
        alignment: Alignment.center,
        child: Builder(builder: (context) {
          if (controller.isLoadingUserList) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else {
            if (controller.isSuccessUserList) {
              return controller.listDataUser.isNotEmpty
                  ? _buildListUser(context,controller ,controller.listDataUser)
                  : const Center(
                      child: Text('Data Not Found'),
                    );
            } else {
              return Center(
                child: PrimaryButton(
                  text: 'Reload',
                  onPressed: () {
                    controller.getUserListProcess();
                  },
                ),
              );
            }
          }
        }),
      );
    });
  }

  Widget _buildListUser(
    BuildContext context,
    ListUserController controller,
    List<DataUser> listDataUser,
  ) {
    return RefreshIndicator(
      onRefresh: ()async{
        controller.getUserListProcess();
      },
      child: ListView.builder(
          itemCount: listDataUser.length,
          padding: const EdgeInsets.symmetric(horizontal: 10),
          itemBuilder: (context, index) {
            return _buildItemUser(listDataUser[index]);
          }),
    );
  }

  Widget _buildItemUser(DataUser itemDataUser) {
    return InkWell(
      onTap: (){
          debugPrint('itemDataUser.id: ${itemDataUser.id}');
        Get.toNamed(UserDetailPage.routeName, arguments: itemDataUser.id);
      },
      child: Container(
        width: double.infinity,
        color: Colors.white,
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.only(top: 10),
        child: Row(
          children: [
            Container(
              width: 50,
              height: 50,
              color: Colors.grey,
              child: Image.network(itemDataUser.avatar),
            ),
            const SizedBox(
              width: 5,
            ),
            Expanded(
              child: Container(
                color: Colors.transparent,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('${itemDataUser.firstName} ${itemDataUser.lastName}'),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(itemDataUser.email)
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
