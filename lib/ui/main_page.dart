import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:user_app/controller/main_controller.dart';

class MainPage extends StatelessWidget {
  static const routeName = '/main-page';
  const MainPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MainController>(builder: (controller) {
      return Scaffold(
        appBar: AppBar(
          title: Obx(() => Text(controller.title.value)),
          centerTitle: true,
        ),

        body: Obx(() => Container(
              width: double.infinity,
              height: double.infinity,
              color: Colors.transparent,
              child: controller.listPage[controller.selectedNavbar.value],
            )),
        bottomNavigationBar: Obx(() => BottomNavigationBar(
              items: controller.listBottomNavigationBarItem,
              currentIndex: controller.selectedNavbar.value,
              onTap: controller.changeSelectedNavBar,
              selectedItemColor: Colors.blue,
              unselectedItemColor: Colors.grey,
              showUnselectedLabels: true,
            )),
      );
    });
  }
}
