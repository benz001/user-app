import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:form_validator/form_validator.dart';
import 'package:get/get.dart';
import 'package:user_app/controller/create_user_controller.dart';
import 'package:user_app/helper/primary_button.dart';
import 'package:user_app/helper/primary_button_loading.dart';
import 'package:user_app/helper/primary_textfield.dart';

class CreateUserPage extends StatelessWidget {
  const CreateUserPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CreateUserController>(builder: (controller) {
      return Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.white,
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              _buildFormCreateUser(controller),
              const SizedBox(
                height: 35,
              ),
              _buildButtonSave(controller),
              const SizedBox(
                height: 35,
              ),
              _buildResult(controller)
            ],
          ),
        ),
      );
    });
  }

  Widget _buildFormCreateUser(CreateUserController controller) {
    return Container(
      width: double.infinity,
      color: Colors.transparent,
      child: Form(
        key: controller.formKey,
        child: Column(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('Name'),
                const SizedBox(
                  height: 5,
                ),
                PrimaryTextField(
                  name: 'name',
                  controller: controller.nameController,
                  hintText: 'Enter Name',
                  fillColor: Colors.white,
                  validator: (v) {
                    debugPrint('validator name: $v');
                    String? result = v as String?;
                    if ((result ?? '').isNotEmpty) {
                      return null;
                    } else {
                      return 'This is required';
                    }
                  },
                )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('Job'),
                const SizedBox(
                  height: 5,
                ),
                PrimaryTextField(
                  name: 'job',
                  controller: controller.jobController,
                  hintText: 'Enter Job',
                  fillColor: Colors.white,
                  validator: (v) {
                    debugPrint('validator job: $v');
                    String? result = v as String?;
                    if ((result ?? '').isNotEmpty) {
                      return null;
                    } else {
                      return 'This is required';
                    }
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _buildButtonSave(CreateUserController controller) {
    return Obx(() => !controller.isLoadingCreateUser.value
        ? Container(
            width: double.infinity,
            height: 45,
            color: Colors.transparent,
            child: PrimaryButton(
              text: 'Save',
              onPressed: () {
                if (controller.formKey.currentState!.validate()) {
                  controller.createUserProcess();
                }
              },
            ))
        : Container(
            width: double.infinity,
            height: 45,
            color: Colors.transparent,
            child: const PrimaryButtonLoading()));
  }

  Widget _buildResult(CreateUserController controller) {
    return Container(
      width: 300,
      color: Colors.grey,
      padding: const EdgeInsets.all(10),
      child: Obx(
         () => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text('Data has been created: '),
              const SizedBox(
                height: 5,
              ),
              Text('ID: ${controller.dataUser.value.id}'),
              const SizedBox(
                height: 5,
              ),
              Text('createdAt: ${controller.dataUser.value.createdAt??''}')
            ],
          )
        
      ),
    );
  }
}
