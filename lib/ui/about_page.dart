import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:user_app/controller/about_user_controller.dart';

class AboutPage extends StatelessWidget {
  const AboutPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AboutUserController>(
      builder: (controller) {
        return Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.white,
          alignment: Alignment.topLeft,
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
               const SizedBox(height: 10,),
             Align(
              alignment: Alignment.topCenter,
              child: _buildImageProfile(controller.pathPhoto)),
             const SizedBox(height: 10,),
             _buildAboutMe(controller.aboutMe),
             const SizedBox(height: 10,),
             _buildSkillList(controller.skillList)
            ],
          ),
        );
      }
    );
  }

  Widget _buildImageProfile(String pathPhoto) {
    return Container(
      width: 100,
      height: 100,
      color: Colors.transparent,
      child: CircleAvatar(
        backgroundImage: AssetImage(pathPhoto),
      ),
    );
  }

  Widget _buildAboutMe(String aboutMe) {
    return Container(
      width: double.infinity,
      color: Colors.transparent,
      child: Column(children: [
        Container(
          width: double.infinity,
          color: Colors.transparent,
          child: const Text('About Me', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
        ),
        const SizedBox(height: 10,),
        Container(
          color: Colors.transparent,
          child:  Text(
              aboutMe),
        )
      ]),
    );
  }

   Widget _buildSkillList(List<String> skillList) {
    return Container(
      width: double.infinity,
      color: Colors.transparent,
      child: Column(children: [
        Container(
          width: double.infinity,
          color: Colors.transparent,
          child: const Text('My Skill', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
        ),
        const SizedBox(height: 5,),
        Container(
          color: Colors.transparent,
          child: ListView.builder(
            itemCount: skillList.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context,index){
            return Container(
              width: double.infinity,
              margin: const EdgeInsets.only(top: 5),
              color: Colors.transparent,
              child: Text( '+${skillList[index].capitalize}'),
            );
          })
        )
      ]),
    );
  }
}
