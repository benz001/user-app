// To parse this JSON data, do
//
//     final userDetailModel = userDetailModelFromJson(jsonString);

import 'dart:convert';

UserDetailModel userDetailModelFromJson(String str) => UserDetailModel.fromJson(json.decode(str));

String userDetailModelToJson(UserDetailModel data) => json.encode(data.toJson());

class UserDetailModel {
    DataUserDetail data;
    SupportDetail support;

    UserDetailModel({
        required this.data,
        required this.support,
    });

    factory UserDetailModel.fromJson(Map<String, dynamic> json) => UserDetailModel(
        data: DataUserDetail.fromJson(json["data"]),
        support: SupportDetail.fromJson(json["support"]),
    );

    Map<String, dynamic> toJson() => {
        "data": data.toJson(),
        "support": support.toJson(),
    };
}

class DataUserDetail {
    int id;
    String email;
    String firstName;
    String lastName;
    String avatar;

    DataUserDetail({
        required this.id,
        required this.email,
        required this.firstName,
        required this.lastName,
        required this.avatar,
    });

    factory DataUserDetail.fromJson(Map<String, dynamic> json) => DataUserDetail(
        id: json["id"],
        email: json["email"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        avatar: json["avatar"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "email": email,
        "first_name": firstName,
        "last_name": lastName,
        "avatar": avatar,
    };
}

class SupportDetail {
    String url;
    String text;

    SupportDetail({
        required this.url,
        required this.text,
    });

    factory SupportDetail.fromJson(Map<String, dynamic> json) => SupportDetail(
        url: json["url"],
        text: json["text"],
    );

    Map<String, dynamic> toJson() => {
        "url": url,
        "text": text,
    };
}
