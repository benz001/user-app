import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';


class PrimaryTextField extends StatelessWidget {
  final String?name;
  final TextEditingController? controller;
  final dynamic validator;
  final String? hintText;
  final Function(String?)? onChanged;
  final bool? obsecureText;
  final TextInputType? keyboardType;
  final Widget? prefixIcon;
  final int? maxLines;
  final Color? fillColor;


  const PrimaryTextField(
      {super.key,
      this.name,
      this.controller,
      this.validator,
      this.hintText,
      this.onChanged,
      this.obsecureText,
      this.keyboardType,
      this.prefixIcon,
      this.maxLines,
      this.fillColor});

  @override
  Widget build(BuildContext context) {
    return FormBuilderTextField(
      name: name??'',
      validator: validator,
      controller: controller,
      onChanged: onChanged,
      obscureText: obsecureText??false,
      keyboardType: keyboardType ?? TextInputType.text,
      maxLines: maxLines??1,
      decoration: InputDecoration(
          hintText: hintText, border: const OutlineInputBorder(), prefixIcon: prefixIcon, fillColor: fillColor),
    );
  }
}
