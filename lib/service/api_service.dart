import 'package:dio/dio.dart';
import 'package:dio_logger/dio_logger.dart';
import 'package:flutter/material.dart';
import 'package:user_app/helper/base_url.dart';
import 'package:user_app/model/create_user_model.dart';
import 'package:user_app/model/user_detail_model.dart';
import 'package:user_app/model/user_model.dart';

class APIService {
  final Dio _dio = Dio()..interceptors.add(dioLoggerInterceptor);
  Future<UserModel> getListUser() async {
    try {
      final response = await _dio.get('${BaseURL.mainURL}/api/users');
      return UserModel.fromJson(response.data);
    } on DioError catch (e) {
      return UserModel(
          page: 0,
          perPage: 0,
          total: 0,
          totalPages: 0,
          data: [],
          support: Support(text: '', url: ''));
    }
  }

  Future<UserDetailModel> geDetailUser(int id) async {
    try {
      final response = await _dio.get('${BaseURL.mainURL}/api/users/$id');
      return UserDetailModel.fromJson(response.data);
    } on DioError catch (e) {
      return UserDetailModel(
          data: DataUserDetail(
              id: 0, email: '', firstName: '', lastName: '', avatar: ''),
          support: SupportDetail(url: '', text: ''));
    }
  }

  Future<CreateUserModel> postCreateUser(String name, String job) async {
    try {
      final response = await _dio.post('${BaseURL.mainURL}/api/users',
          data: {"name": name, "job": job});
      debugPrint('success from postCreateUser');
      return CreateUserModel.fromJson(response.data);
    } on DioError catch (e) {
      debugPrint('error from postCreateUser');
      return CreateUserModel(
          name: '', job: '', id: '', createdAt: DateTime.now());
    }
  }
}
