import 'package:get/get.dart';
import 'package:user_app/controller/user_detail_controller.dart';
import 'package:user_app/controller/main_controller.dart';

class UserDetailBinding extends Bindings {
  @override
  void dependencies() {
   Get.lazyPut(() => UserDetailController());
  }
}