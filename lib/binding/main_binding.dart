import 'package:get/get.dart';
import 'package:user_app/controller/about_user_controller.dart';
import 'package:user_app/controller/create_user_controller.dart';
import 'package:user_app/controller/list_user_controller.dart';
import 'package:user_app/controller/main_controller.dart';

class MainBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(MainController());
    Get.put(ListUserController());
    Get.put(CreateUserController());
    Get.put(AboutUserController());
  }
}
