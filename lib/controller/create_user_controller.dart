import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:user_app/model/create_user_model.dart';
import 'package:user_app/model/user_model.dart';
import 'package:user_app/service/api_service.dart';

class CreateUserController extends GetxController {
  TextEditingController nameController = TextEditingController(text: '');
  TextEditingController jobController = TextEditingController(text: '');

  RxBool isLoadingCreateUser = false.obs;
  RxBool isSuccessCreateUser = false.obs;
  Rx<CreateUserModel> dataUser =
      CreateUserModel(name: '', job: '', id: '', createdAt: null).obs;

  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  void createUserProcess() async {
    isLoadingCreateUser.value = true;
    isSuccessCreateUser.value = false;
    final result = await APIService()
        .postCreateUser(nameController.text, jobController.text);
    if (result.name!.isNotEmpty && result.job!.isNotEmpty) {
      isLoadingCreateUser.value = false;
      isSuccessCreateUser.value = true;
      dataUser.value =  CreateUserModel(name: result.name, job: result.job, id: result.id, createdAt: result.createdAt);
    } else {
      isLoadingCreateUser.value = false;
      isSuccessCreateUser.value = false;
      dataUser.value =
      CreateUserModel(name: '', job: '', id: '', createdAt: null);
      Fluttertoast.showToast(msg: 'Add User Failed');
    }
  }
}
