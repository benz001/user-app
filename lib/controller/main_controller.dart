import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:user_app/controller/list_user_controller.dart';
import 'package:user_app/service/api_service.dart';
import 'package:user_app/ui/about_page.dart';
import 'package:user_app/ui/create_user_page.dart';
import 'package:user_app/ui/list_user_page.dart';

class MainController extends GetxController {
  RxInt selectedNavbar = 0.obs;
  RxString title = 'List Users'.obs; 
  final List<Widget> listPage = [ListUserPage(), CreateUserPage(), AboutPage()];

  final List<BottomNavigationBarItem> listBottomNavigationBarItem = [
    const BottomNavigationBarItem(
      icon: Icon(Icons.list),
      label: 'List User',
    ),
    const BottomNavigationBarItem(
      icon: Icon(Icons.add),
      label: 'New',
    ),
    const BottomNavigationBarItem(icon: Icon(Icons.info), label: 'About')
  ];

  @override
  void onReady() {
    Get.find<ListUserController>().getUserListProcess();
    super.onReady();
  }

  void changeSelectedNavBar(int index) {
    debugPrint('index: $index');
    if (index == 0) {
      debugPrint('getUserListProcess');
      title.value = 'List Users';
      Get.find<ListUserController>().getUserListProcess();
    }else if(index == 1){
      title.value = 'Create';
    }else if(index == 2){
      title.value = 'About';
    }
    selectedNavbar.value = index;
  }
}
