import 'package:get/get.dart';
import 'package:user_app/model/user_model.dart';
import 'package:user_app/service/api_service.dart';

class ListUserController extends GetxController {
  bool isLoadingUserList = false;
  bool isSuccessUserList = false;
  List<DataUser> listDataUser = [];

  void getUserListProcess() async {
    listDataUser.clear();
    isLoadingUserList = true;
    isSuccessUserList = false;
    update();
    final result = await APIService().getListUser();
    if (result.data.isNotEmpty) {
      isLoadingUserList = false;
      isSuccessUserList = true;
      listDataUser = result.data;
      update();
    } else {
      isLoadingUserList = false;
      isSuccessUserList = false;
      update();
    }
  }
}
