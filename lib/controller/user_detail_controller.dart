import 'package:get/get.dart';
import 'package:user_app/model/user_detail_model.dart';
import 'package:user_app/service/api_service.dart';

class UserDetailController extends GetxController {
  bool isLoadingUserDetail = false;
  bool isSuccessUserDetail = false;
  int idUser = Get.arguments;
  UserDetailModel userDetailModel = UserDetailModel(data: DataUserDetail(id: 0, email: '', firstName: '', lastName: '', avatar: ''), support: SupportDetail(url: '', text: ''));
  
  @override
  void onInit() {
    getUserDetailProcess();
    super.onInit();
  }

  void getUserDetailProcess() async {
    isLoadingUserDetail = true;
    isSuccessUserDetail = false;
    update();
    final result = await APIService().geDetailUser(idUser);
    if (result.data.id != 0) {
      isLoadingUserDetail = false;
      isSuccessUserDetail = true;
      userDetailModel = result;
      update();
    } else {
      isLoadingUserDetail = false;
      isSuccessUserDetail = false;
      update();
    }
  }
}
