import 'package:get/get.dart';
import 'package:user_app/binding/user_detail_binding.dart';
import 'package:user_app/binding/main_binding.dart';
import 'package:user_app/ui/user_detail_page.dart';
import 'package:user_app/ui/main_page.dart';

class Routes {
  static List<GetPage<dynamic>>? listPage() {
    return [
      GetPage(name: MainPage.routeName, page: () => const MainPage(), binding: MainBinding()),
      GetPage(
          name: UserDetailPage.routeName, page: () => const UserDetailPage(), binding: UserDetailBinding())
    ];
  }
}
